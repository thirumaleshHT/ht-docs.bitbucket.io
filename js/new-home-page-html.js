$(document).ready(function() {
  $(".header-div-links > li").click(function() {
    $(".header-div-links > li").removeClass("active");
    $(this).addClass("active");
    setTimeout(function() {
      window.scrollBy(0, -80);
    }, 100);
  });

  var division = window.location.href.split("#")[1];
  if (division != null && division != "") {
    $("a[href='#" + division + "'")
      .parent()
      .trigger("click");
  }

  /*var setWidths = function(){
    if($(window).width() < 1600){
      $("body, nav").removeClass("body-container-div-gt-1600").addClass("body-container-div");
      $("div.width-lt-1600, div.width-gt-1600").each(function(){
        $(this).removeClass("width-gt-1600 width-lt-1600").addClass("width-lt-1600");
      });
    }
    else{
      $("body, nav").removeClass("body-container-div").addClass("body-container-div-gt-1600");
      $("div.width-lt-1600, div.width-gt-1600").each(function(){
        $(this).removeClass("width-gt-1600 width-lt-1600").addClass("width-gt-1600");
      });
    }
  }*/

  /*$( window ).resize(function() {
    setWidths();
  });
  setWidths();*/

  $("#ready-for-more-revenue").click(function() {
    window.location.href = "#contact-us";
    $(".header-div-links > li").removeClass("active");
    $("#contact-us").addClass("active");
    setTimeout(function() {
      window.scrollBy(0, -80);
    }, 100);
  });
  $("#see-our-solutions").click(function() {
    window.location.href = "#our-solutions";
    $(".header-div-links > li").removeClass("active");
    $("#our-solutions").addClass("active");
    setTimeout(function() {
      window.scrollBy(0, -80);
    }, 100);
  });
  $("#reach-out-to-us").click(function() {
    window.location.href = "#contact-us";
    $(".header-div-links > li").removeClass("active");
    $("#contact-us").addClass("active");
    setTimeout(function() {
      window.scrollBy(0, -80);
    }, 100);
  });

  $(document).on("scroll", onScroll);

  $(".contact-us-form-submit-button").on("click", function(e) {
    e.preventDefault();
    var firstName = $("#contact-us-form-firstName").val();
    var lastName = $("#contact-us-form-lastName").val();
    var company = $("#contact-us-form-company").val();
    var email = $("#contact-us-form-email").val();
    var comments = $("#contact-us-form-whatYouDo").val();

    if (checkContactUsForm(firstName, lastName, company, email, comments)) {
      sendContactUsMail(firstName, lastName, company, email, comments);
    }
  });
});

function checkContactUsForm(firstName, lastName, company, email, comments) {
  //    var guestPhoneNo_regex = /^\+?[0-9]+[-+. ]?[0-9]*[- ]?[0-9]*$/;
  var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  var emailValid = email.match(email_regex);
  //    var isGuestPhoneValid = (phone.match(guestPhoneNo_regex) == true);

  var errorInDetails = false;

  if (!emailValid) {
    validateField("Please Enter Valid Email to proceed", "contact-us-form-email");
    errorInDetails = true;
    return false;
  }
  //    if (!isGuestPhoneValid) {
  //        validateField("Please fill out valid phone number to proceed", "contact-us-form-phone");
  //        errorInDetails =true;
  //        return false;
  //    }
  return !errorInDetails;
}
function validateField(message, divId) {
  $("#" + divId).focus();
  //    $("#contact-us-error-msg").show();
  //    $("#contact-us-error-msg").text(message);
}
function sendContactUsMail(firstName, lastName, company, email, comments) {
  var data = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    company: company,
    emailId: email,
    comments: comments
  });
  console.log(data);
  $.ajax({
    url: "https://hoteltrader.com/HTCorporate/services/senduscontactmail/" + firstName + "/" + lastName + "/" + company + "/" + email + "/" + comments,
    type: "GET",
    contentType: "application/json",
    success: function(result) {
      $("div.contact-us-form-message").html("<p>Submitted successfully</p>");
    }
  });
}

function onScroll(event) {
  var scrollPosition = $(document).scrollTop();
  $("nav a").each(function() {
    var currentLink = $(this);
    var refElement = $(currentLink.attr("href"));
    if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
      $("nav ul li").removeClass("active");
      currentLink.parent().addClass("active");
    } else {
      currentLink.parent().removeClass("active");
    }
  });
}
